import requests
from django.urls import reverse
from rest_framework.test import APILiveServerTestCase


class EventTest(APILiveServerTestCase):
    fixtures = ['users.json']

    def setUp(self):
        email = 'admin@admin.com'
        password = 'ceaispus'

        data = {
            'email': email,
            'password': password
        }

        url = '{}{}'.format(self.live_server_url, reverse('authentication:email-login'))
        response = requests.post(url, json=data)

        self.headers = {
            'Authorization': 'Token {}'.format(response.json()['token']),
            'Content-Type': 'application/json'
        }

    def test_get_users(self):
        url = '{}{}'.format(self.live_server_url, reverse('events:event-list'))
        response = requests.get(url, headers=self.headers)
        self.assertEqual(response.status_code, 200)
