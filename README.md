# EventX Django Backend

# `.env` support for env vars

# Commands 
#### all the following commands should be run in the root folder of the project

### Install deps 
#### install `pipenv` with `pip` and afterwards
* `pipenv install`
* `pipenv lock` 

### Create admin user
`python3 manage.py createsuperuser`

### Run server
`python3 manage.py runserver 0.0.0.0:8000`

### Create migration
`python3 manage.py createmigrations`

### Make migration
`python3 manage.py migrate`

# Repo steps to make it work:
* connect your project to a `postgre database` (it is configurable from the `.env` file)
* make migration
* run server
* create superuser throw command or normal user from api
